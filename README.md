# csharper

C sharp code that uses System.Reflection to load other C sharp assemblies and run them.

Use https://github.com/Flangvik/NetLoader for a more fully featured version of this project.

Downloading and running seatbelt.exe from memory:

![image](/uploads/9902466890437e4511c0a0336e41a951/image.png)

When compiling C Sharp code to be downloaded and ran by csharper, make sure to make the class that contains the `Main()` method, and the `Main()` method itself is `public`.

![seatbelt_public](/uploads/e0ad42d2d26535bf8e52d9e30cd7c3a7/seatbelt_public.png)

If you're still having issues running assemblies, try changing the architecture of both csharper and the loaded assembly to target x64 builds.